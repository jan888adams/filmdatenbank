<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Anschrift</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <form action="index.php" method="post" class="form-horizontal">

            <div class="form-group">
                <label for="name" class="control-label col-sm-2">Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                </div>
            </div>

            <div class="form-group">
                <label for="genre" class="control-label col-sm-2">Genre:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Genre" name="genre" id="genre">
                </div>
            </div>

            <div class="form-group">
                <label for="director" class="control-label col-sm-2">Regisseur:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="director" name="director" id="director">
                </div>
            </div>

            <div class="form-group">
                <label for="length" class="control-label col-sm-2">Length:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="länge" name="length" id="director">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn  btn-primary" type="submit" name="submitMovie">weiter</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>

