<?php

require_once("DataAccess.php");

/**
 * Class SqlDatabaseEngine
 */
class SqlDatabaseEngine implements DataAccess
{
    /**
     * @var string
     */
    private $servername = "db";
    /**
     * @var string
     */
    private $username = "db";
    /**
     * @var string
     */
    private $password = "db";
    /**
     * @var string
     */
    private $database = "db";
    /**
     * @var mysqli
     */
    private $connection;

    /**
     * SqlDatabaseEngine constructor.
     */
    public function __construct()
    {
        $this->connection = new mysqli($this->servername, $this->username, $this->password, $this->database);
        if ($this->connection->error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }

    /**
     * @return mysqli|string
     */
    public function getConnection()
    {
        return $this->connection;
    }

}