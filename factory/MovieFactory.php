<?php

require_once("IFactory.php");
require_once("model/Movie.php");

class MovieFactory implements IFactory
{
    /**
     * @param array $attributes
     * @return Movie
     */
    public function create(array $attributes): Movie
    {
        $movie = new Movie();

        $movie->setName($attributes['name']);
        $movie->setDirector($attributes['director']);
        $movie->setGenre($attributes['genre']);
        $movie->setLength($attributes['length']);

        return $movie;
    }
}