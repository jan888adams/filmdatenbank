<?php


/**
 * Interface IFactory
 */
Interface IFactory
{
    /**
     * @param array $attributes
     * @return mixed
     */
    function create(Array $attributes) : Movie;

}