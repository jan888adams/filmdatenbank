<?php

require_once("controller/MovieController.php");
require_once("controller/ViewController.php");

if (isset($_POST['submitMovie'])) {

    $attributes = ['name' => $_POST['name'],
        'genre' => $_POST['genre'], 'length' => $_POST['length'], 'director' => $_POST['director']];

    $db = new SqlDatabaseEngine();
    $movieController = new MovieController($db);
    try {
        $movieController->saveMovie($attributes);
    } catch (Exception $e) {
        echo "Datenbakfehler!";
    }

    echo ViewController::render("MovieView");
} else {
    echo ViewController::render("MovieForm");
}
