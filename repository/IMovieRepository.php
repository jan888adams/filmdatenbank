<?php


/**
 * Interface IMovieRepository
 */
interface IMovieRepository
{
    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @param Movie $movie
     * @return mixed
     */
    public function save(Movie $movie);

}