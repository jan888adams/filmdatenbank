<?php

require_once("IMovieRepository.php");
require_once("database/SqlDatabaseEngine.php");

/**
 * Class MovieRepository
 */
class MovieRepository implements IMovieRepository
{
    /**
     * @var SqlDatabaseEngine
     */
    private $db;

    /**
     * MovieRepository constructor.
     * @param DataAccess $connection
     */
    public function __construct(DataAccess $connection)
    {
        $this->db = $connection;
    }

    /**
     * @return bool|mysqli_result
     */
    public function findAll()
    {
        $connection = $this->db->getConnection();

        $sql = "SELECT * FROM movie";

        return $connection->query($sql);
    }

    /**
     * @param Movie $movie
     * @return bool
     */
    public function save(Movie $movie): bool
    {
        $connection = $this->db->getConnection();

        $stmt = $connection->prepare("INSERT INTO movie (`name`, `director`, `genre`, `length`)  VALUES(?, ?, ?, ?)");
        $stmt->bind_param('ssss', $name, $director, $genre, $length);

        $name = $movie->getName();
        $director = $movie->getDirector();
        $genre = $movie->getGenre();
        $length = $movie->getLength();

        $connection->close();

        return $stmt->execute();
    }
}