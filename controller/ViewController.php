<?php

/**
 * Class ViewController
 */
class ViewController
{
    /**
     * @param $v0738238615_name
     * @param array $arguments
     * @return false|string
     */
    public static function render($v0738238615_name, $arguments = [])
    {
        ob_start();
        if (is_array($arguments)) {
            extract($arguments);
        }

        include('view/' . $v0738238615_name . '.php');
        return ob_get_clean();
    }
}