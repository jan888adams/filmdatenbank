<?php

require_once("repository/MovieRepository.php");
require_once("repository/MovieRepository.php");
require_once("factory/MovieFactory.php");

/**
 * Class MovieController
 */
class MovieController
{
    /**
     * @var MovieRepository
     */
    private $repository;

    /**
     * MovieController constructor.
     * @param DataAccess $dataAccess
     */
    public function __construct(DataAccess $dataAccess)
    {
      $this->repository =  new MovieRepository($dataAccess);
    }

    public function showMovies(): void
    {
        $allMovies = $this->repository->findAll();

        while ($row = $allMovies->fetch_assoc()) {
            echo "Film: " . $row["id"] . " " . $row["name"] . " "
                . $row["genre"] . " " . $row["director"] . $row["length"] . "<br>";
        }
    }

    /**
     * @param array $attributes
     */
    public function saveMovie(array $attributes)
    {
        $movieFactory = new MovieFactory();

        $movie = $movieFactory->create($attributes);
        $this->repository->save($movie);
    }

}